package coms.coms.springsecurity.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import coms.coms.springsecurity.model.User;
import coms.coms.springsecurity.web.dto.UserRegistrationDto;



public interface UserService extends UserDetailsService{
	User save(UserRegistrationDto registrationDto);
}
